﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("Queue panel")]
    [SerializeField] GameObject queuePanel;
    [SerializeField] PortraitUI portraitPrefab;

    TurnsManager turnsManager;

    void Start()
    {
        turnsManager = FindObjectOfType<TurnsManager>();

        DestroyAllChildren();
        FillQueuePanel();

        turnsManager.OnNextTurn += OnNextTurn;
    }

    private void FillQueuePanel()
    {
        List<CharacterController> queue = turnsManager.CharactersQueue;

        CreatePortrait(turnsManager.CurrentCharater);
        int currentCharacterIndex = queue.IndexOf(turnsManager.CurrentCharater);
        int nextIndex = currentCharacterIndex + 1;
        if (nextIndex >= queue.Count)
        {
            nextIndex = 0;
        }

        while (nextIndex != currentCharacterIndex)
        {
            CreatePortrait(queue[nextIndex]);
            nextIndex++;

            if (nextIndex >= queue.Count)
            {
                nextIndex = 0;
            }
        }

        //foreach (CharacterController element in queue)
        //{
        //    CreatePortrait(element);
        //}
    }

    private void CreatePortrait(CharacterController element)
    {
        //TODO use pool
        PortraitUI newPortrait = Instantiate(portraitPrefab);
        newPortrait.transform.SetParent(queuePanel.transform);

        newPortrait.InitValues(element);
    }

    void OnNextTurn()
    {
        //queuePanel.transform.GetChild(0).SetAsLastSibling();
        DestroyAllChildren();
        FillQueuePanel();
    }

    private void DestroyAllChildren()
    {
        for (int i = queuePanel.transform.childCount - 1; i >= 0; i--)
        {
            Transform childI = queuePanel.transform.GetChild(i);
            Destroy(childI.gameObject);
        }
    }
}
