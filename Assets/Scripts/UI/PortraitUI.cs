﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PortraitUI : MonoBehaviour
{
    [SerializeField] Image portrait;
    [SerializeField] Image healthBar;

    public void InitValues(CharacterController character)
    {
        portrait.sprite = character.Portrait;
        healthBar.fillAmount = (float) character.Health / character.MaxHealth;
    }
}
