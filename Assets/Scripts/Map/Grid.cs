﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[DisallowMultipleComponent]
public class Grid : MonoBehaviour
{
    [SerializeField] GameObject cellPrefab;
    [SerializeField] int rows = 10;
    [SerializeField] int columns = 10;

    public void GenerateGrid()
    {
        ClearGrid();
        for (int row = 0; row < rows; row++)
        {
            for (int col = 0; col < columns; col++)
            {
                Vector3 position = new Vector3(row, 0, col);
                GameObject newCell = PrefabUtility.InstantiatePrefab(cellPrefab) as GameObject;
                //GameObject newCell = Instantiate(cellPrefab, position, Quaternion.identity);
                newCell.transform.position = position;
                newCell.transform.parent = transform;
                string coordinates = row + "," + col;
                newCell.name = coordinates;
                //newCell.GetComponentInChildren<TextMesh>().text = coordinates;
            }
        }
    }

    private void ClearGrid()
    {
        for(int childIndex = transform.childCount - 1; childIndex >= 0; childIndex--)
        {
            //destroy all children from last to first to prevent errors
            DestroyImmediate(transform.GetChild(childIndex).gameObject);
        }
    }
}
