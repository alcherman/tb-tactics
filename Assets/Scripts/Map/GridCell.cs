﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public class GridCell : MonoBehaviour
{
    public bool isExplored = false;
    public bool isEmpty = true;
    public GridCell exploredFrom;
    public int lengthPath;
    public Color highlitedColor = Color.cyan;

    SpriteRenderer renderer;
    Color defaultColor;

    private void Awake()
    {
        isEmpty = true;
        renderer = GetComponentInChildren<SpriteRenderer>();
        defaultColor = renderer.color;
    }

    public Vector2Int GetCellIndex()
    {
        Vector2Int index = new Vector2Int(
            (int)transform.position.x,
            (int)transform.position.z
            );
        return index;
    }


    public void ResetColor()
    {
        renderer.color = defaultColor;
    }

    public void SetHighlitedColor()
    {
        renderer.color = highlitedColor;
    }

    //public void SetColor(Color color)
    //{
    //    renderer.color = color;
    //}

    public void ResetCell()
    {
        isExplored = false;
        exploredFrom = null;
        lengthPath = 0;
        ResetColor();
    }



}