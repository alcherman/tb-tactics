﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding : MonoBehaviour
{
    Dictionary<Vector2Int, GridCell> grid = new Dictionary<Vector2Int, GridCell>();
    Queue<GridCell> queue = new Queue<GridCell>();

    Vector2Int[] directions =
    {
        Vector2Int.up, //same as new Vector2Int(0, 1)
        Vector2Int.right,
        Vector2Int.down,
        Vector2Int.left,
        Vector2Int.up + Vector2Int.right,
        Vector2Int.right + Vector2Int.down,
        Vector2Int.down + Vector2Int.left,
        Vector2Int.left + Vector2Int.up,
    };

    // Start is called before the first frame update
    void Start()
    {
        LoadGrid();
    }

    public GridCell GetCell(Vector2Int coordinates)
    {
        return grid[coordinates];
    }

    public void ScanGrid(CharacterController character)
    {
        RefreshGrid();

        GridCell firstCell = character.CurrentPosition;
        queue.Enqueue(firstCell);

        while (queue.Count > 0)
        {
            GridCell element = queue.Dequeue();

            if(element == firstCell || element.isEmpty)
            {
                ExploreNeighbours(element);
            }
            element.isExplored = true;

            if (element.lengthPath > character.Speed || !element.isEmpty)
            {
                element.gameObject.SetActive(false);
            }
        }
    }

    public List<GridCell> GetPath(GridCell endCell)
    {
        List<GridCell> path = new List<GridCell>();

        GridCell nextCell = endCell;
        path.Add(endCell);
        while (nextCell.exploredFrom != null)
        {
            path.Add(nextCell.exploredFrom);
            nextCell.SetHighlitedColor();
            nextCell = nextCell.exploredFrom;
        }

        path.Reverse();

        return path;
    }

    //public List<GridCell> FindPath(GridCell startCell, GridCell endCell)
    //{
    //    RefreshGrid();

    //    queue.Enqueue(startCell);

    //    while(queue.Count > 0)
    //    {
    //        GridCell element = queue.Dequeue();
    //        if (element == endCell)
    //        {
    //            //path found
    //            break;
    //        }

    //        ExploreNeighbours(element);
    //        element.isExplored = true;
    //    }

    //    List<GridCell> path = new List<GridCell>();

    //    GridCell nextCell = endCell;
    //    path.Add(endCell);
    //    while (nextCell.exploredFrom != null)
    //    {
    //        path.Add(nextCell.exploredFrom);
    //        nextCell.SetColor(Color.blue);
    //        nextCell = nextCell.exploredFrom;
    //    }

    //    path.Reverse();

    //    return path;
    //}

    private void RefreshGrid()
    {
        queue.Clear();
        foreach (GridCell element in grid.Values)
        {
            element.gameObject.SetActive(true);
            element.ResetCell();
        }
    }

    private void ExploreNeighbours(GridCell cell)
    {
        foreach(Vector2Int element in directions)
        {
            Vector2Int neighbourhCoordinates = cell.GetCellIndex() + element;

            if(grid.ContainsKey(neighbourhCoordinates))
            {
                GridCell neighbour = grid[neighbourhCoordinates];

                if (queue.Contains(neighbour) || neighbour.isExplored)
                {
                    //do nothing
                }
                else
                {
                    queue.Enqueue(neighbour);
                    neighbour.exploredFrom = cell;

                    int length = 0;
                    GridCell nextCell = neighbour;
                    while (nextCell.exploredFrom != null)
                    {
                        length++;
                        nextCell = nextCell.exploredFrom;
                    }

                    neighbour.lengthPath = length;
                }
            }
        }
    }

    private void LoadGrid()
    {
        GridCell[] cells = FindObjectsOfType<GridCell>(); //find all grid cells
        foreach(GridCell element in cells)
        {
            grid.Add(element.GetCellIndex(), element);
        }
    }
}
