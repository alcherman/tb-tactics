﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CharacterController : MonoBehaviour//, IComparable<CharacterController>
{
    public Action<CharacterController> OnCharacterDie;

    [SerializeField] int speed;
    [SerializeField] float movementSpeed = 1;
    [SerializeField] int initiative;
    [SerializeField] Sprite portrait;
    [SerializeField] int maxHealth = 100;
    [SerializeField] GameObject damageText;

    Pathfinding pathfinding;
    Animator animator;

    CharacterController attackTarget;
    Action finishedAction;

    public int Speed
    {
        get { return speed; }
    }

    public Sprite Portrait
    {
        get { return portrait; }
    }

    public int Initiative
    {
        get { return initiative; }
    }

    public int MaxHealth
    {
        get { return maxHealth; }
    }


    public int Health
    {
        get;
        private set;
    }

    public GridCell CurrentPosition
    {
        get;
        private set;
    }

    //public int CompareTo(CharacterController other)
    //{
    //    if (other == null)
    //    {
    //        return 1;
    //    }

    //    //return this.initiative.CompareTo(other.initiative);
    //    return other.initiative.CompareTo(this.initiative);
    //}

    //the same
    //public int GetSpeed()
    //{
    //    return speed;
    //}

    // Start is called before the first frame update
    void Start()
    {
        pathfinding = FindObjectOfType<Pathfinding>();
        animator = GetComponentInChildren<Animator>();

        Vector2Int snappedPosition = new Vector2Int();
        snappedPosition.x = Mathf.RoundToInt(transform.position.x);
        snappedPosition.y = Mathf.RoundToInt(transform.position.z);

        CurrentPosition = pathfinding.GetCell(snappedPosition);
        CurrentPosition.isEmpty = false;

        transform.position = CurrentPosition.transform.position;

        Health = UnityEngine.Random.Range(10, 90);
    }

    public void SetIdleAnimation()
    {
        print("Set Idle");
        animator.SetTrigger("Idle");
    }

    public void MoveTo(GridCell cell, Action finishedMovement)
    {
        List<GridCell> path = pathfinding.GetPath(cell);

        if(path.Count > Speed)
        {
            path = path.Take(Speed + 1).ToList();
        }

        CurrentPosition.isEmpty = true;
        CurrentPosition = path.Last();
        CurrentPosition.isEmpty = false;

        StartCoroutine(MoveInPath(path, finishedMovement));
    }

    public void AttackCharacter(CharacterController target, Action finishedAttack)
    {
        attackTarget = target;
        finishedAction = finishedAttack;

        if (target.CurrentPosition.lengthPath > 1)
        {
            //move and attack
            MoveTo(target.CurrentPosition.exploredFrom, AttackTarget);
        }
        else
        {
            AttackTarget();
        }

    }

    void AttackTarget()
    {
        transform.LookAt(attackTarget.transform.position);
        animator.SetTrigger("Attack");
        //print(animator.GetCurrentAnimatorClipInfo(0)[0].clip.length);

        StartCoroutine(ActionWithDelay(finishedAction, 3));
    }

    public void AttackHitMoment()
    {
        attackTarget.TakeDamage();
        attackTarget = null;

    }

    public void TakeDamage()
    {
        //health > 0
        //animator.SetTrigger("Damage");

        //health <= 0

        //TODO Use pool
        Instantiate(damageText, transform.position, Quaternion.identity);
        Die();
    }

    void Die()
    {
        animator.SetTrigger("Death");

        Collider collider = GetComponent<Collider>();
        collider.enabled = false;

        CurrentPosition.isEmpty = true;

        OnCharacterDie(this);
    }

    IEnumerator ActionWithDelay(Action finishedAction, float delay)
    {
        yield return new WaitForSeconds(delay);
        finishedAction();
    }

    IEnumerator MoveInPath(List<GridCell> waypoints, Action finishedMovement)
    {
        print("Set Move");
        animator.SetTrigger("Move");
        //foreach(GridCell element in waypoints)
        for(int i=0; i < waypoints.Count; i++)
        {
            GridCell element = waypoints[i];
            Vector3 target = element.transform.position;

            while (Vector3.Distance(transform.position, target) > 0.005f)
            {
                transform.position = Vector3.MoveTowards(transform.position, target, movementSpeed * Time.deltaTime);
                yield return null;
            }


            int nextElementIndex = i + 1;
            if (nextElementIndex < waypoints.Count)
            {
                GridCell nextElement = waypoints[i + 1];
                transform.LookAt(nextElement.transform.position);
            }
            yield return null;
        }

        //finished movement
        animator.SetTrigger("Stand");
        finishedMovement();
    }

}
