﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAI : MonoBehaviour
{
    CharacterController characterController;
    TurnsManager turnsManager;

    // Start is called before the first frame update
    void Awake()
    {
        characterController = GetComponent<CharacterController>();
        turnsManager = FindObjectOfType<TurnsManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MakeTurn(Action finishedTurn)
    {
        CharacterController nearestCharacter = null;

        //try to find nearest player's character
        foreach(CharacterController element in turnsManager.PlayerCharacters)
        {
            if (nearestCharacter == null)
            {
                //assign first element
                nearestCharacter = element;
            }
            else
            {
                int lengthToNearest = nearestCharacter.CurrentPosition.lengthPath;
                int lengthToElement = element.CurrentPosition.lengthPath;

                if (lengthToElement < lengthToNearest)
                {
                    nearestCharacter = element;
                }
            }
        }

        if(characterController.Speed >= nearestCharacter.CurrentPosition.lengthPath - 1)
        {
            //Attack
            characterController.AttackCharacter(nearestCharacter, finishedTurn);
        }
        else
        {
            //Move
            characterController.MoveTo(nearestCharacter.CurrentPosition, finishedTurn);
        }

    }

}
