﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TurnsManager : MonoBehaviour
{
    public Action OnNextTurn = delegate { };


    [Header("Players")]
    [SerializeField] List<CharacterController> playerCharacters;
    [Header("Enemies")]
    [SerializeField] List<CharacterController> enemyCharacters;

    [Header("Cursors")]
    [SerializeField] Texture2D cursorNormal;
    [SerializeField] Texture2D cursorDisabled;
    [SerializeField] Texture2D cursorAttack;
    [SerializeField] Texture2D cursorMove;

    Pathfinding pathfinding;
    bool allowInput;

    CharacterController currentCharacter;
    List<CharacterController> charactersQueue;

    public CharacterController CurrentCharater
    {
        get { return currentCharacter; }
    }

    public List<CharacterController> CharactersQueue
    {
        get
        {
            return charactersQueue;
        }
    }

    public List<CharacterController> PlayerCharacters
    {
        get { return playerCharacters; }
    }

    // Start is called before the first frame update
    void Start()
    {
        SetCursor(cursorNormal);

        pathfinding = FindObjectOfType<Pathfinding>();

        //CharacterController[] objects = FindObjectsOfType<CharacterController>();
        charactersQueue = new List<CharacterController>();
        charactersQueue.AddRange(playerCharacters);
        charactersQueue.AddRange(enemyCharacters);

        //charactersQueue.Sort();
        //charactersQueue.Sort((x, y) => y.Initiative.CompareTo(x.Initiative));

        charactersQueue = charactersQueue.OrderByDescending(x => x.Initiative).ToList();

        foreach (CharacterController element in charactersQueue)
        {
            element.OnCharacterDie += CharacterDied;
        }

        foreach (CharacterController element in enemyCharacters)
        {
            element.gameObject.AddComponent<CharacterAI>();
        }

        NextTurn();
    }

    void SetCursor(Texture2D cursor)
    {
        Cursor.SetCursor(cursor, Vector2.zero, CursorMode.Auto);
    }

    void CharacterDied(CharacterController character)
    {
        charactersQueue.Remove(character);
        playerCharacters.Remove(character);
        enemyCharacters.Remove(character);

        if(playerCharacters.Count == 0)
        {
            Debug.Log("Defeated");
        }

        if(enemyCharacters.Count == 0)
        {
            Debug.Log("Victory");
        }
    }

    void NextTurn()
    {
        int currentIndex = charactersQueue.IndexOf(currentCharacter);
        int newIndex = currentIndex + 1;
        if (newIndex >= charactersQueue.Count)
        {
            newIndex = 0;
            //new round
        }
        currentCharacter = charactersQueue[newIndex];
        currentCharacter.SetIdleAnimation();
        pathfinding.ScanGrid(currentCharacter);
        OnNextTurn();

        if (!IsPlayerCharacter(currentCharacter))
        {
            //AI's Turn
            allowInput = false;
            currentCharacter.GetComponent<CharacterAI>().MakeTurn(FinishedMovement);
        }
        else
        {
            //Player's turn
            allowInput = true;
        }
    }

    GridCell lastCellRaycast;

    // Update is called once per frame
    void Update()
    {
        if (allowInput)
        {
            if (lastCellRaycast != null)
            {
                lastCellRaycast.ResetColor();
            }
            SetCursor(cursorNormal);

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                GridCell cell = hit.transform.GetComponent<GridCell>();
                if (cell != null)
                {
                    SetCursor(cursorMove);
                    if (Input.GetMouseButtonDown(0))
                    {
                        currentCharacter.MoveTo(cell, FinishedMovement);
                        allowInput = false;
                        SetCursor(cursorDisabled);
                    }
                    cell.SetHighlitedColor();
                    lastCellRaycast = cell;
                }
                else
                {
                    CharacterController character = hit.transform.GetComponent<CharacterController>();
                    if(character != null && character != currentCharacter && !IsInOnTeam(currentCharacter, character))
                    {
                        SetCursor(cursorAttack);
                        if (Input.GetMouseButtonDown(0))
                        {
                            if (currentCharacter.Speed >= character.CurrentPosition.lengthPath - 1)
                            {
                                //attack
                                allowInput = false;
                                currentCharacter.AttackCharacter(character, FinishedMovement);
                                SetCursor(cursorDisabled);
                            }
                        }
                    }
                }
            }
        }
    }

    bool IsInOnTeam(CharacterController character1, CharacterController character2)
    {
        bool isInOnTeam = (playerCharacters.Contains(character1) && playerCharacters.Contains(character2))
                           || (enemyCharacters.Contains(character1) && enemyCharacters.Contains(character2));
        return isInOnTeam;

    }

    public bool IsPlayerCharacter(CharacterController character)
    {
        //bool isEnemy = enemyCharacters.Contains(character);
        //return isEnemy;
        return playerCharacters.Contains(character);
    }

    void FinishedMovement()
    {
        NextTurn();
    }

}
